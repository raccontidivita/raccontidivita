<!DOCTYPE html>
<html>
  
 <head>
	  <title>Concorso - #raccontidivita :: Veneta Cucine ::</title>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <link rel="stylesheet" type="text/css" href="css/style.css" />
	  <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
	
 </head>

	 

 <body>
	 <div class="container">
	 	<div id="bg2">
	 	  <a href="index.php">
	 	   <div class="top_line"><img src="img/top.png" alt="top" width="395" height="89" /></div>
	 	 </a>
	 	   <div class="tag"><img src="img/tag.png" alt="tag" width="134" height="81" /></div>
	 	 
	 	   <div class="paragraph">
		 	   <h2>Inviaci la tua foto!</h2>
		 	   <p>La foto deve essere in formato JPG/JPEG ed il peso non deve superare i 2Mb.<br/>
				  Si consiglia di caricare foto con orientamento orizzontale.<br/>
				  La pubblicazione delle stesse sarà a discrezione di Veneta Cucine.</p>
		   </div>
		   
		   <div class="form">
		   		<form action="inviofoto.php" method="post">
		   		   <div class="col-2">
			   			<p>Nome: <span>nome proveniente da FacebookLogin</span></p>
			   			<p>Cognome: <span>cognome proveniente da FacebookLogin</span></p>
			   			<p>e-mail: <span>e-mail proveniente da FacebookLogin</span></p>
			   			<input value="Città" name="citta" type="text"><br/>
			   			<input value="Rivenditore" name="rivenditore" type="text"><br/>
						 
						 
						  <select required="" data-placeholder="Modello cucina" name="modello">
								<option value="">Modello cucina</option>
								<option value="-1">Altro modello</option>
								<option value="45">Ca' Veneta</option>
								<option value="19">California</option>
								<option value="20">Carrera</option>
								<option value="48">Carrera.Go</option>
								<option value="233">Dialogo</option>
								<option value="234">Dialogo ShellSystem</option>
								<option value="33">Diamante</option>
								<option value="54">Ecocompatta</option>
								<option value="235">Elegante</option>
								<option value="236">Elegante ShellSystem</option>
								<option value="36">Ethica Decorativo</option>
								<option value="237">Ethica.go</option>
								<option value="27">Extra</option>
								<option value="29">Extra Avant</option>
								<option value="59">Extra.Go</option>
								<option value="10">Gretha</option>
								<option value="52">Liquida Condense</option>
								<option value="51">Liquida Flipper</option>
								<option value="62">Liquida Frame</option>
								<option value="53">Liquida Light</option>
								<option value="44">Memory</option>
								<option value="60">Mirabeau</option>
								<option value="14">Newport</option>
								<option value="26">Oyster</option>
								<option value="37">Oyster Decorativo</option>
								<option value="57">Ri-flex</option>
								<option value="15">Roccafiorita</option>
								<option value="55">Start-Time</option>
								<option value="56">Start-Time.Go</option>
								<option value="61">Start-Time.Go 28</option>
								<option value="63">Tablet</option>
								<option value="64">Tablet.Go</option>
								<option value="38">Tulipano</option>
								<option value="17">Verdiana</option>
								<option value="18">Villa d'Este</option>
								<option value="46">Vintage</option>
								<option value="58">Vintage ShellSystem</option>
						</select>
		   			</div>
		   		</form>
		   
		   </div>
		   
	 	</div>
	 </div>
 </body>
</html>