<!DOCTYPE html>
<html>
  
 <head>
	  <title>Concorso - #raccontidivita :: Veneta Cucine ::</title>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <link rel="stylesheet" type="text/css" href="css/style.css" />
	  <link href='http://fonts.googleapis.com/css?family=Over+the+Rainbow' rel='stylesheet' type='text/css'>
	
 </head>

	 

 <body>
	 <div class="container">
	 	<div id="bg2">
	
	 	   <div class="top_line"><img src="img/top.png" alt="top" width="395" height="89" /></div>
	 	 
	 	   <div class="tag"><img src="img/tag.png" alt="tag" width="134" height="81" /></div>
	 	 
	 	   <div class="paragraph">
		 	   <h2>Benvenuto!</h2>
		 	   <p>La cucina è il luogo di vita vissuta, è l'ambiente della casa dedicato alla convivialità, alla condivisione e allo stare insieme.
		 	   Vivi la tua Veneta Cucine e raccontaci un momento speciale nella tua cucina!</p>
		   </div>
		   
		   <div class="col-3 l">
		   
		   </div>
		   
		   <div class="col-3 l">
		   
		   </div>
		   
		   <div class="col-3 r">
		   
		   </div>
		   
		   <div class="paragraph">
		   		<p>Condividi la foto, fatti votare e vinci!<br/>
		   		   I 5 pi&ugrave; votati entro il 1 marzo 2015 riceveranno il Kitchen Assistent di Electrolux!
		   		</p>
		   </div>	
		   
		   <div class="paragraph">		   
			   <a href="inviofoto.php" class="l"><div class="button">Carica <br/>la tua foto</div></a>
			   <a href="vota.php" class="r"><div class="button">Vota</div></a>
		   </div>
		   		 	
		</div>
	 </div>
 </body>
</html>